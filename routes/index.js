var express = require("express");
var router = express.Router();

const fs = require("fs");
const bodyParser = require("body-parser");

var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

const { getUser } = require("../services/user.service");

/* GET home page. */
router.get("/", function(req, res, next) {
  res.send("Express");
});

router.get("/user", function(req, res, next) {
  fs.readFile("./db/userlist.json", "utf8", function(error, data) {
    if (error) throw error;
    res.send(JSON.parse(data));
  });
});

router.get("/user/:id", function(req, res, next) {
  let id = req.params.id;
  let users;
  fs.readFile("./db/userlist.json", "utf8", function(error, data) {
    if (error) throw error;
    users = JSON.parse(data);
    let user = users.filter(user => {
      if (user["_id"] == id) {
        return user;
      }
    });
    res.send(user[0]);
  });
});

router.post("/user", function(req, res, next) {
  let reqBody = getUser(req.body);
  let bodyUser = req.body;
  fs.readFile("./db/userlist.json", "utf8", function(error, data) {
    if (error) throw error;
    users = JSON.parse(data);
    if (reqBody) {
      let { _id, ...newUser } = bodyUser;
      newUser._id = (users.length + 1).toString();
      users.push(newUser);
      fs.writeFile(
        "./db/userlist.json",
        JSON.stringify(users),
        "utf-8",
        function(err) {
          if (err) throw err;
          console.log("Додано нового юзера");
        }
      );
      res.send("Юзера успішно додано");
    } else {
      res.status(400).send(`Some error`);
    }
  });
});

router.put("/user/:id", function(req, res, next) {
  let newData = req.body;
  let id = req.params.id;
  fs.readFile("./db/userlist.json", "utf8", function(error, data) {
    if (error) throw error;
    users = JSON.parse(data);
    let updatedUsers = users.map(user => {
      if (user["_id"] == id) {
        user = { ...user, ...newData };
      }
      return user;
    });
    fs.writeFile(
      "./db/userlist.json",
      JSON.stringify(updatedUsers),
      "utf-8",
      function(err) {
        if (err) throw err;
        console.log("Дані оновлено");
      }
    );
    res.send(updatedUsers);
  });
});

router.delete("/user/:id", function(req, res, next) {
  let idToDelete = req.params.id;
  fs.readFile("./db/userlist.json", "utf8", function(error, data) {
    if (error) throw error;
    users = JSON.parse(data);
    let updatedUsers = users.filter(user => user["_id"] !== idToDelete);
    fs.writeFile(
      "./db/userlist.json",
      JSON.stringify(updatedUsers),
      "utf-8",
      function(err) {
        if (err) throw err;
        console.log("Дані оновлено");
      }
    );
    res.send(updatedUsers);
  });
});

module.exports = router;
