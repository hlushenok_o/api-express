/* services/user.service.js */

const getName = user => {
  if (user) {
    return user.name;
  } else {
    return null;
  }
};

const getUser = user => {
  if (user) {
    return user;
  } else {
    return null;
  }
};

module.exports = {
  getName,
  getUser
};
